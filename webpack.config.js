var webpack = require('webpack');

module.exports = {
    entry: {
        app: './main.js',
        vendor: ['jquery', 'paper'],
    },
    output: {
        filename: 'main.min.js'
    },
    module: {
        loaders: [
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
      },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                loader: 'file'
      }
    ]
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            filename: 'vendor.bundle.js'
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        })
  ]
};
